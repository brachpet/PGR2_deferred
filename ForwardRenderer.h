#pragma once
#include "Renderer.h"

class ForwardRenderer : public Renderer {
	ShaderProgram forwardProgram;
	ShaderProgram forwardBlendProgram;
	GLuint pointLightUniformBuffer = 0;

	void initShaders();
	void initGeometry();

	void updatePointLightBuffer();

public:
	ForwardRenderer(Scene * scene);
	ForwardRenderer();
	~ForwardRenderer();

	void init();
	void render();
};