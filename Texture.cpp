#include <IL/il.h>

#include <iostream>

#include "Texture.h"
#include "common.h"

Texture::Texture(const char * textureFile)
{
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	// set linear filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	//chci generovat mipmapy
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	ILuint img_id;
	ilGenImages(1, &img_id); // generate one image ID (name)
	ilBindImage(img_id); // bind that generated id

	// set origin to LOWER LEFT corner (the orientation which OpenGL uses)
	ilEnable(IL_ORIGIN_SET);
	ilSetInteger(IL_ORIGIN_MODE, IL_ORIGIN_LOWER_LEFT);

	if (ilLoadImage(textureFile) == IL_FALSE) {
		ilDeleteImages(1, &img_id);
		LOG("cannot load image from path: ");
		LOG(textureFile);
		return;
	}

	LOG("texture has been loaded \n");

	// if the image was correctly loaded, we can obtain some informatins about our image
	ILint width = ilGetInteger(IL_IMAGE_WIDTH);
	ILint height = ilGetInteger(IL_IMAGE_HEIGHT);
	ILenum format = ilGetInteger(IL_IMAGE_FORMAT);
	// there are many possible image formats and data types
	// we will convert all image types to RGB or RGBA format, with one byte per channel
	unsigned Bpp = ((format == IL_RGBA || format == IL_BGRA) ? 4 : 3);
	char * data = new char[width * height * Bpp];
	// this will convert image to RGB or RGBA, one byte per channel and store data to our array
	ilCopyPixels(0, 0, 0, width, height, 1, Bpp == 4 ? IL_RGBA : IL_RGB, IL_UNSIGNED_BYTE, data);
	// image data has been copied, we dont need the DevIL object anymore
	ilDeleteImages(1, &img_id);

	// bogus ATI drivers may require this call to work with mipmaps
	glEnable(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D, 0, Bpp == 4 ? GL_RGBA : GL_RGB, width, height, 0, Bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);

	// free our data (they were copied to OpenGL)
	delete[] data;

	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}
