#pragma once
#include "Renderer.h"

static const float screenVertices[] = {
	-1.0f, 1.0f,
	 1.0f, 1.0f,
	-1.0f,-1.0f,
	 1.0f,-1.0f,
};

static const int screenIndices[] = {
	1,0,2,
	1,2,3
};


class DeferredRenderer : public Renderer {
	ShaderProgram geometryPassProgram;
	ShaderProgram screenSpaceProgram;
	ShaderProgram directionalLightProgram;
	ShaderProgram pointLightProgram;
	ShaderProgram forwardBlendProgram;

	GLuint pointLightUniformBuffer = 0;

	GLuint frameBuffer = 0;
	GLuint depthTexture = 0;
	GLuint colorTexture = 0;
	GLuint normalTexture = 0;
	GLuint positionTexture = 0;
	GLuint tangentTexture = 0;
	GLuint normalMattexture = 0;
	GLuint specularTexture = 0;

	GLuint screenVertexArrayObject;
	GLuint screenVertexBufferObject;
	GLuint screenElementBufferObject;


	void initShaders();
	void initGeometry();
	void initFrameBuffer();
	void initLightVolumes();

	void geometryPass();
	void lightPass();
	void blendPass();

	void updatePointLightBuffer();

public:
	DeferredRenderer(Scene * scene);
	DeferredRenderer();
	~DeferredRenderer();

	void init();
	void render();

	void setViewportDimensions(int width, int height);
};