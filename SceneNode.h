#pragma once
#include <glm/glm.hpp>

#include <list>

class SceneNode {
public:
	std::list<SceneNode*> children;
	SceneNode* parent;

	glm::vec3 scale;
	glm::vec3 rotation;				/// in euler angles, degrees
	glm::vec3 translation;

	glm::mat4 localTransformation;	/// trasnlation * rotation * scale
	glm::mat4 matrixToRoot;			/// cashed matrix, doesnt need to compute it every frame

	glm::mat4 localNormalMatrix;
	glm::mat4 normalMatrixToRoot;

	SceneNode(SceneNode * parent);
	SceneNode();
	virtual ~SceneNode();

	//absolute setting of object transformation
	void translateTo(glm::vec3 offset);
	void scaleTo(glm::vec3 scaling);
	void rotateTo(glm::vec3 eulerAngles);

	//relative setting of object transformation
	void translateBy(glm::vec3 direction);		/// translates object in specified direction
	void scaleBy(glm::vec3 scaling);			/// adds scaling factor to an existing one
	void rotateBy(glm::vec3 eulerAngles);		/// adds rotation to existing one

	void updateLocalTransform();
	virtual void computeMatrixToRoot();

	virtual void print();
};