#pragma once
#include "Scene.h"

#define MAX_POINT_LIGHTS_COUNT 256

class Renderer {
public:
	Scene * scene;
	int viewportWidth;
	int viewportHeight;

	int numOfrenderedLights = 15;
	bool sunOn = true;

	Renderer(Scene * scene);
	Renderer();
	virtual ~Renderer();

	virtual void init() = 0;
	virtual void render() = 0;

	virtual void setViewportDimensions(int width, int height);
};