#pragma once
#include "GL/glew.h"
#include <assimp/scene.h>

#include "ShaderProgram.h"
#include "SceneNode.h"
#include "Material.h"

static const GLfloat triangleData[] = {
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	0.0f,  1.0f, 0.0f,
};

static const GLint triangleIndices[] = {
	0,1,2,
};

class Mesh {
	GLuint vertexArrayObject;
	GLuint vertexBufferObject;
	GLuint elementBufferObject;

	int numOfFaces;
	int numOfVertices;
	int stride;

public:
	Material * material;
	std::vector<SceneNode*> locationsInScene;

	Mesh();

	void loadData();	/// loading of default object (traingle)
	void loadData(const aiMesh & mesh);
	void loadFromFile(const char* path);

	void attachToScene(SceneNode* sceneNode);
	void setupVertexArray(const ShaderProgram & shaderProgram);
	void render();
};
