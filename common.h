#pragma once
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480


#include <iostream>

#ifdef _DEBUG 
#define LOG(msg) \
std::cout << msg
#else
#define LOG(msg)
#endif // _DEBUG




typedef enum { FORWARD = 1, DEFERRED, DEFERRED_OPTIMIZED } RendererType;