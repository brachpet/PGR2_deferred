#version 420 core
in vec3 vertexNormal;
in vec2 vertexUvs;
in vec3 vertexTangent;
in vec3 vertexPosition;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 normal;
layout (location = 2) out vec4 position;
layout (location = 3) out vec4 tangent;
layout (location = 4) out vec4 materialNormal;
layout (location = 5) out vec4 specular;

uniform sampler2D textureDiffuse;
uniform sampler2D textureOpacity;
uniform sampler2D textureNormal;
uniform sampler2D textureSpecular;

uniform  bool  useOpacity  = false;
uniform  bool  useNormal   = false; 
uniform  bool  useSpecular = false; 

void main(){

    if (useOpacity){
        vec4 textureOpacity = texture(textureOpacity, vertexUvs);
        if (textureOpacity.x < 0.9) discard;
    }

    //layout 0
    vec4 textureColor = texture(textureDiffuse, vertexUvs);
    color = textureColor;

    //layout 1
    normal = vec4(vertexNormal, 0.0);

    //layout 2
    position = vec4(vertexPosition, 1.0);

    //layout 3
    tangent = vec4(vertexTangent, 0.0);

    //layout 4
    if (useNormal){
        materialNormal = texture(textureNormal, vertexUvs);
    } else {
        materialNormal = vec4(0.5,0.5,1.0,0.0);
    }

    //layout 5
    specular = texture(textureSpecular, vertexUvs);
}