#version 420 core

layout (location = 0) out vec4 color;

uniform vec2 windowSize;
uniform sampler2D textureDiffuse;

void main(){
    vec2 coords = vec2(gl_FragCoord.x / windowSize.x, gl_FragCoord.y / windowSize.y);
    color = texture(textureDiffuse, coords);
}