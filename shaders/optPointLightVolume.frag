#version 420 core

layout (location = 0) out vec4 color;

uniform sampler2D textureDiffuse;
uniform sampler2D textureNormal;
uniform sampler2D texturePosition;
uniform vec2 windowSize;

uniform vec3 lightPosition;
uniform vec3 lightColor;

uniform vec3 cameraPosition;

vec3 CalcPointLight(float lightAttenuation, vec3 normal, vec3 position){
    vec3 L = lightPosition - position;
    L = normalize(L);
    vec3 V = cameraPosition - position;
    V = normalize(V);
    vec3 R = normal * ( 2.0f * dot(L, normal) ) - L;

    vec3 lightDiffuse = lightColor * max((dot(L, normal)), 0.0f);
    float d = distance(lightPosition, position);
    float attenuationFactor = 1.0f / (0.1 + 0.5*d + 3*d*d);
    lightDiffuse *= attenuationFactor;

    float x = 3.0-d;
    x = min(x, 1.0);
    x = max(x, 0.0);
    return mix(vec3(0,0,0),lightDiffuse,x);
}

void main(){
    vec2 coords = vec2(gl_FragCoord.x / windowSize.x, gl_FragCoord.y / windowSize.y);

    //vec3 normal = texture(textureNormal, coords).xyz;
    vec3 normal = texelFetch(textureNormal, ivec2(gl_FragCoord.xy), 0).xyz;
    //vec3 diffuse = texture(textureDiffuse, coords).xyz;
    vec3 diffuse = texelFetch(textureDiffuse, ivec2(gl_FragCoord.xy), 0).xyz;
    //vec3 position = texture(texturePosition, coords).xyz;
    vec3 position = texelFetch(texturePosition, ivec2(gl_FragCoord.xy), 0).xyz;

    vec3 lightIntensity = CalcPointLight(1.0, normal, position);

    lightIntensity *= diffuse;

    color = vec4(lightIntensity,1); //vec4(sun,1);
}