#version 420 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uvs;
layout(location = 3) in vec3 tangent;

out vec3 vertexNormal;
out vec2 vertexUvs;
out vec3 vertexTangent;
out vec3 vertexPosition;

uniform mat4 MVP;
uniform mat4 normalMatrix;
uniform mat4 modelMatrix;

void main(){
    vertexNormal   = (normalMatrix * vec4(normal, 1.0)).xyz;
    vertexUvs      = uvs;
    vertexTangent  = (normalMatrix * vec4(tangent, 1.0)).xyz;
    vertexPosition = (modelMatrix * vec4(position, 1.0)).xyz;

	gl_Position = MVP * vec4(position, 1.0);
}