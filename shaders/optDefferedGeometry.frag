#version 420 core
in vec3 vertexNormal;
in vec2 vertexUvs;
in vec3 vertexTangent;
in vec3 vertexPosition;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 normal;
layout (location = 2) out vec4 position;
layout (location = 3) out vec4 specular;

uniform sampler2D textureDiffuse;
uniform sampler2D textureOpacity;
uniform sampler2D textureNormal;
uniform sampler2D textureSpecular;

uniform  bool  useOpacity  = false;
uniform  bool  useNormal   = false; 
uniform  bool  useSpecular = false; 

vec3 CalcBumpedNormal()
{
    vec3 Normal = normalize(vertexNormal);
    vec3 Tangent = normalize(vertexTangent);
    Tangent = normalize(Tangent - dot(Tangent, Normal) * Normal);
    vec3 Bitangent = cross(Tangent, Normal);
    vec3 BumpMapNormal = texture(textureNormal, vertexUvs).xyz;
    BumpMapNormal = 2.0 * BumpMapNormal - vec3(1.0, 1.0, 1.0);
    vec3 NewNormal;
    mat3 TBN = mat3(Tangent, Bitangent, Normal);
    NewNormal = TBN * BumpMapNormal;
    NewNormal = normalize(NewNormal);
    return NewNormal;
}

void main(){

    if (useOpacity){
        vec4 textureOpacity = texture(textureOpacity, vertexUvs);
        if (textureOpacity.x < 0.9) discard;
    }

    //layout 0
    vec4 textureColor = texture(textureDiffuse, vertexUvs);
    color = textureColor;

    //layout 1
    normal = vec4(vertexNormal, 0.0);
    if (useNormal){
        normal = vec4(CalcBumpedNormal(), 0.0);
    }

    //layout 2
    position = vec4(vertexPosition, 1.0);

    //layout 3
    specular = texture(textureSpecular, vertexUvs);
}