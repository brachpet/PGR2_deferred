#version 420 core

layout (location = 0) out vec4 color;

uniform sampler2D textureDiffuse;
uniform sampler2D textureNormal;
uniform sampler2D textureMatNormal;
uniform sampler2D textureTangent;
uniform vec2 windowSize;

uniform vec3 lightDirection;
uniform vec3 lightColor;

vec3 CalcBumpedNormal(vec3 Normal, vec3 Tangent, vec3 BumpMapNormal)
{
    Tangent = normalize(Tangent - dot(Tangent, Normal) * Normal);
    vec3 Bitangent = cross(Tangent, Normal);
    BumpMapNormal = 2.0 * BumpMapNormal - vec3(1.0, 1.0, 1.0);
    vec3 NewNormal;
    mat3 TBN = mat3(Tangent, Bitangent, Normal);
    NewNormal = TBN * BumpMapNormal;
    NewNormal = normalize(NewNormal);
    return NewNormal;
}

void main(){
    vec2 coords = vec2(gl_FragCoord.x / windowSize.x, gl_FragCoord.y / windowSize.y);

    vec3 normal = texture(textureNormal, coords).xyz;
    vec3 diffuse = texture(textureDiffuse, coords).xyz;
    vec3 BumpMapNormal = texture(textureMatNormal, coords).xyz;
    vec3 tangent = texture(textureTangent, coords).xyz;

    normal = CalcBumpedNormal(normal, tangent, BumpMapNormal);

    float diffuseFactor = dot(normalize(normal), -lightDirection);
    diffuseFactor = max(diffuseFactor, 0.2);
    vec3 sun = diffuseFactor * lightColor * diffuse;

    color = vec4(sun,1);
}