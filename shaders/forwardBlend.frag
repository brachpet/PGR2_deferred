#version 420 core
in vec3 vertexNormal;
in vec2 vertexUvs;
in vec3 vertexTangent;
in vec3 vertexPosition;

out vec4 color;

uniform sampler2D textureDiffuse;
uniform sampler2D textureOpacity;
uniform sampler2D textureNormal;
uniform sampler2D textureSpecular;

uniform  bool  useOpacity  = false;
uniform  bool  useNormal   = false; 
uniform  bool  useSpecular = false; 

struct SunLight{
    vec3 direction;
    vec3 color;
};

uniform SunLight sunLight;

uniform int numOfPointLights = 2;
layout (binding = 0) uniform PointLightsBO{
    vec3 position[256];
    vec3 color[256];
    float attenuation[256];
} pointLights;

uniform vec3 cameraPosition;

vec3 CalcPointLight(vec3 lightPosition, vec3 lightColor, float lightAttenuation, vec3 normal){
    vec3 L = lightPosition - vertexPosition;
    L = normalize(L);
    vec3 V = cameraPosition - vertexPosition;
    V = normalize(V);
    vec3 R = normal * ( 2.0f * dot(L, normal) ) - L;

    vec3 lightDiffuse = lightColor * max((dot(L, normal)), 0.0f);
    float d = distance(lightPosition, vertexPosition);
    float attenuationFactor = 1.0f / (0.1 + 0.5*d + 3*d*d);
    lightDiffuse *= attenuationFactor;

    float x = 3.0-d;
    x = min(x, 1.0);
    x = max(x, 0.0);
    return mix(vec3(0,0,0),lightDiffuse,x);
}

vec3 CalcBumpedNormal()
{
    vec3 Normal = normalize(vertexNormal);
    vec3 Tangent = normalize(vertexTangent);
    Tangent = normalize(Tangent - dot(Tangent, Normal) * Normal);
    vec3 Bitangent = cross(Tangent, Normal);
    vec3 BumpMapNormal = texture(textureNormal, vertexUvs).xyz;
    BumpMapNormal = 2.0 * BumpMapNormal - vec3(1.0, 1.0, 1.0);
    vec3 NewNormal;
    mat3 TBN = mat3(Tangent, Bitangent, Normal);
    NewNormal = TBN * BumpMapNormal;
    NewNormal = normalize(NewNormal);
    return NewNormal;
}

void main(){

    vec3 normal = vertexNormal;
    if (useNormal){
        normal = CalcBumpedNormal();
    }

  

    vec4 textureColor = texture(textureDiffuse, vertexUvs);
    

    vec3 lightComplete = vec3(0.0);

    for(int i = 0; i < numOfPointLights; i++){
        lightComplete += CalcPointLight(pointLights.position[i], pointLights.color[i], 1.0, normal);
    }

    

    //tmp sunlight
    float diffuseFactor = dot(normalize(normal), -sunLight.direction);
    diffuseFactor = max(diffuseFactor, 0.2);
    vec3 sun = diffuseFactor * sunLight.color;
    color = textureColor * vec4(sun + lightComplete, 1);   

    if (useOpacity){
        vec4 textureOpacity = texture(textureOpacity, vertexUvs);
        color.w = textureOpacity.x;
    }

/*
    if (useSpecular){
        vec4 specular = texture(textureSpecular, vertexUvs);
        vec4 lightSpecular = specular * pow(max(dot(R, V), 0.0f), 50);
        lightSpecular *= attenuationFactor;
        color = color + lightSpecular;
    }*/ 
    //color = vec4(vertexPosition, 1.0);
}