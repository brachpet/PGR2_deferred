#version 420 core

layout (location = 0) out vec4 color;

uniform sampler2D textureDiffuse;
uniform sampler2D textureNormal;
uniform vec2 windowSize;

uniform vec3 lightDirection;
uniform vec3 lightColor;

void main(){
    vec2 coords = vec2(gl_FragCoord.x / windowSize.x, gl_FragCoord.y / windowSize.y);

    //vec3 normal = texture(textureNormal, coords).xyz;
    //vec3 diffuse = texture(textureDiffuse, coords).xyz;
    vec3 normal = texelFetch(textureNormal, ivec2(gl_FragCoord.xy), 0).xyz;
    vec3 diffuse = texelFetch(textureDiffuse, ivec2(gl_FragCoord.xy), 0).xyz;

    float diffuseFactor = dot(normalize(normal), -lightDirection);
    diffuseFactor = max(diffuseFactor, 0.2);
    vec3 sun = diffuseFactor * lightColor * diffuse;

    color = vec4(sun,1);
}