#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>

#include "Scene.h"
#include "Texture.h"
#include "common.h"

void Scene::loadFromFile(const char * path)
{
	Assimp::Importer importer;

	const aiScene * scn = importer.ReadFile(path, 0
		| aiProcess_Triangulate
		| aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
		| aiProcess_GenSmoothNormals        // Calculate normals per vertex.
		| aiProcess_GenUVCoords
		| aiProcess_JoinIdenticalVertices
		| aiProcess_CalcTangentSpace
	);

	if (!scn) {
		LOG("Failed to init scene from file \n");
		return;
	}

	if (scn->mNumMeshes == 0) {
		LOG("Scene contains zero meshes \n");
		return;
	}

	//cteni jednotlivych materialu
	int numberOfMaterials = scn->mNumMaterials;
	for (int i = 0; i < numberOfMaterials; i++)
	{
		const aiMaterial * assimpMaterial = scn->mMaterials[i];

		Material * material = new Material();

		aiString texturePath;

		unsigned int numTextures = assimpMaterial->GetTextureCount(aiTextureType_DIFFUSE);
		if (assimpMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0 && assimpMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == AI_SUCCESS)
		{
			Texture * texture = new Texture(texturePath.C_Str());
			material->textureDiffuse = texture;
		}

		LOG("num of diffuse textures: ");
		LOG(numTextures);
		LOG("\n");

		numTextures = assimpMaterial->GetTextureCount(aiTextureType_HEIGHT);
		if (assimpMaterial->GetTextureCount(aiTextureType_HEIGHT) > 0 && assimpMaterial->GetTexture(aiTextureType_HEIGHT, 0, &texturePath) == AI_SUCCESS)
		{
			Texture * texture = new Texture(texturePath.C_Str());
			material->textureNormal = texture;
		}
		else {
			LOG("num of normal textures: ");
			LOG(numTextures);
			LOG("\n");
		}

		numTextures = assimpMaterial->GetTextureCount(aiTextureType_OPACITY);
		if (assimpMaterial->GetTextureCount(aiTextureType_OPACITY) > 0 && assimpMaterial->GetTexture(aiTextureType_OPACITY, 0, &texturePath) == AI_SUCCESS)
		{
			Texture * texture = new Texture(texturePath.C_Str());
			material->textureOpacity = texture;
		}
		else {
			LOG("num of opacity textures: ");
			LOG(numTextures);
			LOG("\n");
		}

		numTextures = assimpMaterial->GetTextureCount(aiTextureType_SPECULAR);
		if (assimpMaterial->GetTextureCount(aiTextureType_SPECULAR) > 0 && assimpMaterial->GetTexture(aiTextureType_SPECULAR, 0, &texturePath) == AI_SUCCESS)
		{
			Texture * texture = new Texture(texturePath.C_Str());
			material->textureSpecular = texture;
		}
		else {
			LOG("num of specular textures: ");
			LOG(numTextures);
			LOG("\n");
		}

		materials.push_back(material);
	}


	//cteni jednotlivych meshu a vyroba grafu sceny pro kazdy takovy mesh
	int numberOfMeshes = scn->mNumMeshes;
	for (int i = 0; i < numberOfMeshes; i++)
	{
		const aiMesh * assimpMesh = scn->mMeshes[i];

		LOG("loading of: ");
		LOG(assimpMesh->mName.C_Str());
		LOG(", num of vertices: ");
		LOG(assimpMesh->mNumVertices);
		LOG("\n");
		
		Mesh* mesh = new Mesh();
		mesh->loadData(*assimpMesh);	// nahraje data meshe a zabali je do bufferu
		mesh->attachToScene(sceneRoot); // pripoji mesh ke korenu sceny

		mesh->material = materials[assimpMesh->mMaterialIndex];

		meshes.push_back(mesh);			// zaradi mesh do knihovny meshu sceny
	}

}

Scene::Scene()
{
	sceneRoot = new SceneNode();
}

Scene::~Scene()
{

	LOG("deleting scene \n");

	std::vector<Material*>::iterator matIt;
	for (matIt = materials.begin(); matIt != materials.end(); ++matIt) {
		delete *matIt;
	}

	std::vector<Mesh*>::iterator meshIt;
	for (meshIt = meshes.begin(); meshIt != meshes.end(); ++meshIt) {
		delete *meshIt;
	}

	std::vector<PointLight*>::iterator lightIt;
	for (lightIt = pointLights.begin(); lightIt != pointLights.end(); ++lightIt) {
		delete *lightIt;
	}

	std::vector<Mesh*>::iterator blendMeshIt;
	for (blendMeshIt = blendedMeshes.begin(); blendMeshIt != blendedMeshes.end(); ++blendMeshIt) {
		delete *blendMeshIt;
	}

	delete sceneRoot;
}
