#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <iostream>

#include "SceneNode.h"
#include "common.h"

void SceneNode::updateLocalTransform()
{
	//scale
	localTransformation = glm::scale(glm::mat4(1.0f), scale);

	//rotation
	glm::quat quat = glm::quat(rotation);
	glm::mat4 rotationMatrix = glm::toMat4(quat);
	localTransformation = rotationMatrix * localTransformation;

	//translation
	localTransformation = glm::translate(localTransformation, translation);

	//normal matrix
	localNormalMatrix = glm::transpose(glm::inverse(localTransformation));

	computeMatrixToRoot();
}

SceneNode::SceneNode(SceneNode * parent) : parent(parent), scale(glm::vec3(1.0f, 1.0f, 1.0f)), 
										   rotation(glm::vec3(0.0f, 0.0f, 0.0f)), translation(glm::vec3(0.0f, 0.0f, 0.0f)), 
										   localTransformation(glm::mat4(1.0f)), matrixToRoot(glm::mat4(1.0f)), localNormalMatrix(glm::mat4(1.0f)),
										   normalMatrixToRoot(glm::mat4(1.0f))
{
	LOG("creating SceneNode \n");
	if (parent != nullptr)
		parent->children.push_back(this);
	computeMatrixToRoot();
}

SceneNode::SceneNode() : parent(nullptr), scale(glm::vec3(1.0f,1.0f,1.0f)), rotation(glm::vec3(0.0f,0.0f,0.0f)), 
						 translation(glm::vec3(0.0f,0.0f,0.0f)), localTransformation(glm::mat4(1.0f)), matrixToRoot(glm::mat4(1.0f)), 
						 localNormalMatrix(glm::mat4(1.0f)), normalMatrixToRoot(glm::mat4(1.0f))
{

	LOG("creating root SceneNode \n");
	computeMatrixToRoot();
}

SceneNode::~SceneNode()
{
	LOG("deleting SceneNode \n");
	std::list<SceneNode*>::iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		delete *it;
	}
}

void SceneNode::translateTo(glm::vec3 offset)
{
	translation = offset;
	updateLocalTransform();
}

void SceneNode::scaleTo(glm::vec3 scaling)
{
	scale = scaling;
	updateLocalTransform();
}

void SceneNode::rotateTo(glm::vec3 eulerAngles)
{
	rotation = eulerAngles;
	updateLocalTransform();
}

void SceneNode::translateBy(glm::vec3 direction)
{
	translation += direction;
	updateLocalTransform();
}

void SceneNode::scaleBy(glm::vec3 scaling)
{
	scale += scaling;
	updateLocalTransform();
}

void SceneNode::rotateBy(glm::vec3 eulerAngles)
{
	rotation += eulerAngles;
	updateLocalTransform();
}

void SceneNode::computeMatrixToRoot()
{
	if (parent != nullptr) {
		matrixToRoot       = localTransformation * parent->matrixToRoot;
		normalMatrixToRoot = localNormalMatrix * parent->normalMatrixToRoot;
	}
	else {
		matrixToRoot       = localTransformation;
		normalMatrixToRoot = localNormalMatrix;
	}
	std::list<SceneNode*>::iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		(*it)->computeMatrixToRoot();
	}
}

void SceneNode::print()
{
	printf("printing Scene Node \n");
	std::list<SceneNode*>::iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		(*it)->print();
	}
}
