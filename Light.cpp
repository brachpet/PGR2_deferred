#include <iostream>

#include "Light.h"

Light::Light() : locationInScene(nullptr)
{
}

void Light::attachToScene(SceneNode * sceneNode)
{
	locationInScene = sceneNode;
}

PointLight::PointLight() : Light()
{
}

glm::vec4 PointLight::getPositionInWorldSpace()
{
	glm::mat4 localMat = locationInScene->matrixToRoot;
	return localMat * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
}

void PointLight::initVolume(const ShaderProgram & shaderProgram)
{
	glGenVertexArrays(1, &volumeVertexArrayObject);
	glGenBuffers(1, &volumeVertexBufferObject);
	glGenBuffers(1, &volumeElementBufferObject);

	glBindBuffer(GL_ARRAY_BUFFER, volumeVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pointLightVolumeVertices), pointLightVolumeVertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, volumeElementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(pointLightVolumeIndices), pointLightVolumeIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(volumeVertexArrayObject);
	glBindBuffer(GL_ARRAY_BUFFER, volumeVertexBufferObject);

	//position pointer
	if (shaderProgram.shaderLocations.attributPosition != -1) {
		glEnableVertexAttribArray(shaderProgram.shaderLocations.attributPosition);
		glVertexAttribPointer(shaderProgram.shaderLocations.attributPosition, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}

	//unbiding
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void PointLight::renderVolume()
{
	glBindVertexArray(volumeVertexArrayObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, volumeElementBufferObject);
	glDrawElements(GL_TRIANGLES, 3 * 18, GL_UNSIGNED_INT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

glm::mat4 PointLight::getSpriteRotation(glm::vec3 cameraPosition)
{
	glm::vec4 lightPos = getPositionInWorldSpace();
	glm::vec3 n = cameraPosition - glm::vec3(lightPos.x, lightPos.y, lightPos.z);
	n = glm::normalize(n);
	glm::vec3 u = glm::vec3(0, 0, 1);

	glm::vec3 r = glm::cross(u, n);
	r = glm::normalize(r);
	u = glm::cross(n, r);
	u = glm::normalize(u);

	glm::mat3 spriteRot = glm::mat3(r, u, n);

	glm::mat4 spriteMat = glm::mat4(spriteRot);
	spriteMat[3].w = 1.0f;

	return spriteMat;
}

DirectionalLight::DirectionalLight() : Light(), direction(glm::vec3(1.0f,0.0f,0.0f))
{
}

void DirectionalLight::initVolume(const ShaderProgram & shaderProgram)
{
	//std::cout << "directional light volume inited" << std::endl;

	glGenVertexArrays(1, &volumeVertexArrayObject);
	glGenBuffers(1, &volumeVertexBufferObject);
	glGenBuffers(1, &volumeElementBufferObject);

	glBindBuffer(GL_ARRAY_BUFFER, volumeVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(directionalLightVolumeVertices), directionalLightVolumeVertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, volumeElementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(directionalLightVolumeIndices), directionalLightVolumeIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(volumeVertexArrayObject);
	glBindBuffer(GL_ARRAY_BUFFER, volumeVertexBufferObject);

	//position pointer
	if (shaderProgram.shaderLocations.attributPosition != -1) {
		glEnableVertexAttribArray(shaderProgram.shaderLocations.attributPosition);
		glVertexAttribPointer(shaderProgram.shaderLocations.attributPosition, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}

	//unbiding
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void DirectionalLight::renderVolume()
{
	glBindVertexArray(volumeVertexArrayObject);						//setup atribut pointeru
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, volumeElementBufferObject);	//indexovy buffer
	glDrawElements(GL_TRIANGLES, 3 * 2, GL_UNSIGNED_INT, 0);		//kresleni pomoci indexu
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
