#pragma once
#include "Texture.h"

class Material {
public:
	Texture * textureDiffuse;
	Texture * textureNormal;
	Texture * textureSpecular;
	Texture * textureOpacity;

	Material();
	~Material();

	void loadFromFile(const char* path);

};