#pragma once
#include "Renderer.h"
#include "common.h"

class RenderManager {
	GLuint64 gpu_start = 0;
	GLuint64 gpu_stop  = 0;
	GLuint   qid[2]    = { 0 };

public:
	Renderer* forwardRenderer;
	Renderer* deferredRenderer;
	Renderer* optimizedDeferredRenderer;

	Renderer* activeRenderer;

	Scene* scene;

	float windowWidth;
	float windowHeight;

	RenderManager();
	~RenderManager();

private:
	void initScene();
	void initRenderers();

public:
	void init();
	void setNumOfRenderedLights(int count);
	void setSunLight(bool isOn);
	double getGPUFrameTime();
	void setCameraDirection(float cursorX, float cursorY);
	void moveCameraForward(float delta);
	void moveCameraSideways(float delta);
	void setViewportDimensions(float width, float height);
	void render();
	void updateScene();
	void switchToRenderer(RendererType type);
};