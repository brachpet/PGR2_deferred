#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "Camera.h"
#include "common.h"

CameraNode::CameraNode(SceneNode * parent) : SceneNode(parent), inverseMatrixToRoot(glm::mat4(1.0f)), direction(glm::vec3(1.0f,0.0f,0.0f)), FOV(45.0f), sensitivity(5.0f)
{
	LOG("creating Camera node \n");
	verticalAxisTop = 1.0f;
	verticalAxisBottom = 0.0f;
	computeMatrixToRoot();
}

CameraNode::CameraNode() : SceneNode(), inverseMatrixToRoot(glm::mat4(1.0f)), direction(glm::vec3(1.0f, 0.0f, 0.0f)), FOV(45.0f), sensitivity(5.0f)
{
	LOG("creating Camera node \n");
	verticalAxisTop = 1.0f;
	verticalAxisBottom = 0.0f;
	computeMatrixToRoot();
}

CameraNode::~CameraNode()
{
	LOG("deleting Camera node \n");
	std::list<SceneNode*>::iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		delete *it;
	}
}

void CameraNode::computeMatrixToRoot()
{
	if (parent != nullptr) {
		matrixToRoot = localTransformation * parent->matrixToRoot;
	}
	else {
		matrixToRoot = localTransformation;
	}
	std::list<SceneNode*>::iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		(*it)->computeMatrixToRoot();
	}

	computeInverseMatrix();
}

void CameraNode::computeInverseMatrix()
{
	inverseMatrixToRoot = glm::inverse(matrixToRoot);
}

void CameraNode::setCameraDirection(glm::vec3 direction)
{
	this->direction = direction;
}

void CameraNode::setCameraDirection(float windowX, float windowY)
{
	direction.x = sin(windowX * sensitivity);
	direction.y = cos(windowX * sensitivity);

	if (verticalAxisTop <= windowY) {
		verticalAxisTop = windowY;
		verticalAxisBottom = verticalAxisTop - 1.0f;
	} else if (verticalAxisBottom >= windowY) {
		verticalAxisBottom = windowY;
		verticalAxisTop = verticalAxisBottom + 1.0f;
	}
	direction.z = (verticalAxisTop - windowY - 0.5f) * sensitivity;

	direction = glm::normalize(direction);
}

void CameraNode::moveForward(float delta)
{
	translateBy(direction * delta);
}

void CameraNode::moveSideways(float delta)
{
	glm::vec3 dir = glm::cross(direction, glm::vec3(0.0f, 0.0f, 1.0f));
	dir = glm::normalize(dir);
	translateBy(dir * delta);
}

glm::mat4 CameraNode::getViewMatrix() const
{
	glm::mat4 view = glm::lookAt(
		glm::vec3(0.0f),	// position of camera
		direction,			// point we are looking at
		glm::vec3(0, 0, 1)	// up vector
	);

	return  view * inverseMatrixToRoot;
}

glm::vec3 CameraNode::getPositionInWordSpace() const
{
	glm::vec4 pos = glm::vec4(0, 0, 0, 1);
	pos = matrixToRoot * pos;

	return glm::vec3(pos.x, pos.y, pos.z);
}

void CameraNode::print()
{
	printf("printing Camera Node \n");
	std::list<SceneNode*>::iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		(*it)->print();
	}
}
