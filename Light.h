#pragma once
#include "SceneNode.h"
#include "ShaderProgram.h"

class Light {
public:
	glm::vec3 lightColor;
	SceneNode * locationInScene;
	Light();

	void attachToScene(SceneNode * sceneNode);

	virtual void renderVolume() = 0;
	virtual void initVolume(const ShaderProgram & shaderProgram) = 0;
};

static const float pointLightVolumeVertices[] = {
	0.0f, 1.0f,
	0.3090170026893479f, 0.9510565135936411f,
	0.587785266437776f, 0.8090169840977831f,
	0.8090170097906934f, 0.5877852310745185f,
	0.9510565271012029f, 0.3090169611173454f,
	1.0f, 0.0f,
	0.9510565000860774f, -0.30901704426134974f,
	0.8090170284743339f, -0.5877852053586913f,
	0.5877851957112599f, -0.809017035483602f,
	0.3090170329201147f, -0.9510565037710687f,
	0.0f, -1.0f,
	-0.3090169724585808f, -0.9510565234162125f,
	-0.5877853371642876f, -0.8090169327119581f,
	-0.8090168509681034f, -0.5877854496750099f,
	-0.951056480440929f, -0.3090171047228822f,
	-1.0f, 0.0f,
	-0.9510564730709448f, 0.30901712740535175f,
	-0.8090168369495607f, 0.5877854689698682f,
	-0.587785317869427f, 0.8090169467304975f,
	-0.30901694977611f, 0.9510565307861931f,
};

static const int pointLightVolumeIndices[] = {
	0, 2, 1,
	0, 3, 2,
	0, 4, 3,
	0, 5, 4,
	0, 6, 5,
	0, 7, 6,
	0, 8, 7,
	0, 9, 8,
	0, 10, 9,
	0, 11, 10,
	0, 12, 11,
	0, 13, 12,
	0, 14, 13,
	0, 15, 14,
	0, 16, 15,
	0, 17, 16,
	0, 18, 17,
	0, 19, 18,
};

class PointLight : public Light {
	GLuint volumeVertexArrayObject   = 0;
	GLuint volumeVertexBufferObject  = 0;
	GLuint volumeElementBufferObject = 0;

public:
	PointLight();

	glm::vec4 getPositionInWorldSpace();
	void initVolume(const ShaderProgram & shaderProgram);
	void renderVolume();
	glm::mat4 getSpriteRotation(glm::vec3 cameraPosition);
};

static const float directionalLightVolumeVertices[] = {
	-1.0f, 1.0f,
	1.0f, 1.0f,
	-1.0f,-1.0f,
	1.0f,-1.0f
};

static const int directionalLightVolumeIndices[] = {
	1,0,2,
	1,2,3
};

class DirectionalLight : public Light {
	GLuint volumeVertexArrayObject   = 0;
	GLuint volumeVertexBufferObject  = 0;
	GLuint volumeElementBufferObject = 0;

public:
	glm::vec3 direction;

	DirectionalLight();

	void initVolume(const ShaderProgram & shaderProgram);
	void renderVolume();
};

