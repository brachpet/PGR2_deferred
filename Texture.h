#pragma once
#include "GL/glew.h"

class Texture {
public:
	GLuint textureId;

	Texture(const char * textureFile);
};