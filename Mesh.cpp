#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>

#include <iostream>

#include "Mesh.h"
#include "common.h"

Mesh::Mesh() : material(nullptr)
{
	//vygenerovani bufferu
	glGenVertexArrays(1, &vertexArrayObject);
	glGenBuffers(1, &vertexBufferObject);
	glGenBuffers(1, &elementBufferObject);

}

void Mesh::loadData()
{
	numOfFaces    = 1;
	numOfVertices = 3;
	stride        = 0;

	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleData), triangleData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numOfVertices * sizeof(unsigned) * numOfFaces, triangleIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Mesh::loadData(const aiMesh & mesh)
{
	numOfFaces    = mesh.mNumFaces;
	numOfVertices = mesh.mNumVertices;
	stride        = 3 + 3 + 2 + 3;

	//vertex buffer
	float * meshData = new float[numOfVertices * stride];
	for (int i = 0; i < numOfVertices; i++)
	{
		//pozice
		meshData[stride * i + 0] = mesh.mVertices[i].x;
		meshData[stride * i + 1] = mesh.mVertices[i].y;
		meshData[stride * i + 2] = mesh.mVertices[i].z;

		//normala
		meshData[stride * i + 3] = mesh.mNormals[i].x;
		meshData[stride * i + 4] = mesh.mNormals[i].y;
		meshData[stride * i + 5] = mesh.mNormals[i].z;

		//uv
		meshData[stride * i + 6] = mesh.mTextureCoords[0][i].x;
		meshData[stride * i + 7] = mesh.mTextureCoords[0][i].y;

		//tangents
		meshData[stride * i + 8]  = mesh.mTangents[i].x;
		meshData[stride * i + 9]  = mesh.mTangents[i].y;
		meshData[stride * i + 10] = mesh.mTangents[i].z;
	}
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * numOfVertices * stride, meshData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	delete[] meshData;

	
	//index buffer
	unsigned * indices = new unsigned[numOfFaces * 3];
	for (unsigned f = 0; f < numOfFaces; ++f) {
		indices[f * 3 + 0] = mesh.mFaces[f].mIndices[0];
		indices[f * 3 + 1] = mesh.mFaces[f].mIndices[1];
		indices[f * 3 + 2] = mesh.mFaces[f].mIndices[2];
	}
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * 3 * numOfFaces, indices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	delete[] indices;

}

void Mesh::loadFromFile(const char * path)
{
	Assimp::Importer importer;

	const aiScene * scn = importer.ReadFile(path, 0
		| aiProcess_Triangulate
		| aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
		| aiProcess_GenSmoothNormals        // Calculate normals per vertex.
		| aiProcess_GenUVCoords
		| aiProcess_JoinIdenticalVertices
		| aiProcess_CalcTangentSpace
	);

	if (!scn) {
		LOG("Failed to init scene from file \n");
		return;
	}

	if (scn->mNumMeshes == 0) {
		LOG("Scene contains zero meshes \n");
		return;
	}

	const aiMesh * assimpMesh = scn->mMeshes[0];

	LOG("loading of: ");
	LOG(assimpMesh->mName.C_Str());
	LOG(", num of vertices: ");
	LOG(assimpMesh->mNumVertices);
	LOG("\n");

	loadData(*assimpMesh);	// nahraje data meshe a zabali je do bufferu
}

void Mesh::attachToScene(SceneNode * sceneNode)
{
	locationsInScene.push_back(sceneNode);
}

void Mesh::setupVertexArray(const ShaderProgram & shaderProgram)
{	
	glBindVertexArray(vertexArrayObject);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);

	//position pointer
	if (shaderProgram.shaderLocations.attributPosition != -1) {
		glEnableVertexAttribArray(shaderProgram.shaderLocations.attributPosition);
		glVertexAttribPointer(shaderProgram.shaderLocations.attributPosition, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), 0);
	}

	//normal pointer
	if (shaderProgram.shaderLocations.attributNormal != -1) {
		glEnableVertexAttribArray(shaderProgram.shaderLocations.attributNormal);
		glVertexAttribPointer(shaderProgram.shaderLocations.attributNormal, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(3 * sizeof(float)));
	}

	//uvs pointer
	if (shaderProgram.shaderLocations.attributUVs != -1) {
		glEnableVertexAttribArray(shaderProgram.shaderLocations.attributUVs);
		glVertexAttribPointer(shaderProgram.shaderLocations.attributUVs, 2, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(6 * sizeof(float)));
	}

	//tangent pointer
	if (shaderProgram.shaderLocations.attributTangent != -1) {
		glEnableVertexAttribArray(shaderProgram.shaderLocations.attributTangent);
		glVertexAttribPointer(shaderProgram.shaderLocations.attributTangent, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(8 * sizeof(float)));
	}

	//unbiding
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}

void Mesh::render()
{
	glBindVertexArray(vertexArrayObject);						//setup atribut pointeru
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObject);	//indexovy buffer
	glDrawElements(GL_TRIANGLES, 3 * numOfFaces, GL_UNSIGNED_INT, 0);		//kresleni pomoci indexu

	//unbinding
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
