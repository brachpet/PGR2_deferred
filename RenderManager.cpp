#include <time.h>

#include "RenderManager.h"
#include "ForwardRenderer.h"
#include "DeferredRenderer.h"
#include "DeferredRendererOpt.h"
#include "common.h"

SceneNode* rotatingNode;
SceneNode* rotatingNode2;
SceneNode* rotatingNode3;

RenderManager::RenderManager() : forwardRenderer(nullptr), deferredRenderer(nullptr), optimizedDeferredRenderer(nullptr), scene(nullptr), windowWidth(WINDOW_WIDTH), windowHeight(WINDOW_HEIGHT)
{
}

RenderManager::~RenderManager()
{
	activeRenderer = nullptr;
	delete forwardRenderer;
	delete deferredRenderer;
	delete optimizedDeferredRenderer;
	delete scene;
}

void RenderManager::initScene()
{
	//stavba sceny ze souboru
	scene = new Scene();
	scene->activeCamera = new CameraNode(scene->sceneRoot);
	scene->loadFromFile("models/spoza.obj");
	scene->activeCamera->translateTo(glm::vec3(0, 0, 2));

	//smerove svetlo
	SceneNode * sunLightNode = new SceneNode(scene->sceneRoot);
	DirectionalLight * sunLight = new DirectionalLight();
	sunLight->direction = glm::vec3(-1.0f, -1.0, -1.0f);
	sunLight->lightColor = glm::vec3(0.878f, 0.95f, 1.0f);
	//sunLight->lightColor = glm::vec3(0.0f);
	sunLight->attachToScene(sunLightNode);
	scene->directionalLights.push_back(sunLight);

	rotatingNode = new SceneNode(scene->sceneRoot);
	rotatingNode2 = new SceneNode(scene->sceneRoot);
	rotatingNode3 = new SceneNode(scene->sceneRoot);

	
	//vytvoreni bodovych svetel
	for (int i = 0; i < 256; i++)
	{
		SceneNode * lightNode;
		if (i % 3 == 0) {
			lightNode = new SceneNode(rotatingNode);
		}
		else if (i % 2 == 0) {
			lightNode = new SceneNode(rotatingNode2);
		}
		else {
			lightNode = new SceneNode(rotatingNode3);
		}

		PointLight * pointLight = new PointLight();

		//random pozice
		float u = ((float)rand() / (RAND_MAX));
		float v = ((float)rand() / (RAND_MAX));
		float w = ((float)rand() / (RAND_MAX));
		glm::vec3 lightPosition = glm::vec3(u*24.0f - 12.0f, v*10.0f - 5.0f, w*7.0f);
		lightNode->translateTo(lightPosition);
		
		pointLight->lightColor = glm::vec3(u, v, w);
		pointLight->attachToScene(lightNode);

		scene->pointLights.push_back(pointLight);
	}
	LOG("----------------------\n");

	//sphere nodes init
	SceneNode * sphereNode = new SceneNode(rotatingNode);
	sphereNode->translateTo(glm::vec3(0, 0, 1));
	Mesh * mesh = new Mesh();
	mesh->loadFromFile("models/sphere.obj");
	Material* material = new Material();
	material->loadFromFile("models/sphere.obj");
	mesh->material = material;
	mesh->attachToScene(sphereNode);
	scene->meshes.push_back(mesh);
	scene->materials.push_back(material);

	SceneNode * sphereNode2 = new SceneNode(rotatingNode2);
	sphereNode2->translateTo(glm::vec3(1, 1, 1));
	Mesh * mesh2 = new Mesh(*mesh);
	mesh2->locationsInScene.clear();
	mesh2->attachToScene(sphereNode2);
	scene->meshes.push_back(mesh2);

	SceneNode * sphereNode3 = new SceneNode(rotatingNode3);
	sphereNode3->translateTo(glm::vec3(1, 2, 1));
	Mesh * mesh3 = new Mesh(*mesh);
	mesh3->locationsInScene.clear();
	mesh3->attachToScene(sphereNode3);
	scene->meshes.push_back(mesh3);

	//blended nodes init
	SceneNode * blendNode = new SceneNode(scene->sceneRoot);
	Mesh * blendMesh = new Mesh();
	blendMesh->loadFromFile("models/blended.obj");
	Material* blendMat = new Material();
	blendMat->loadFromFile("models/blended.obj");
	blendMesh->material = blendMat;
	blendMesh->attachToScene(blendNode);
	//scene->meshes.push_back(blendMesh);
	scene->blendedMeshes.push_back(blendMesh);
	scene->materials.push_back(blendMat);

}

void RenderManager::initRenderers()
{
	forwardRenderer = new ForwardRenderer(scene);
	forwardRenderer->init();

	deferredRenderer = new DeferredRenderer(scene);
	deferredRenderer->init();

	optimizedDeferredRenderer = new DeferredRendererOpt(scene);
	optimizedDeferredRenderer->init();

	activeRenderer = forwardRenderer;
}

void RenderManager::init()
{
	initScene();
	initRenderers();
}

void RenderManager::setNumOfRenderedLights(int count)
{
	if (forwardRenderer != nullptr) {
		forwardRenderer->numOfrenderedLights = count;
	}
	if (deferredRenderer != nullptr) {
		deferredRenderer->numOfrenderedLights = count;
	}
	if (optimizedDeferredRenderer != nullptr) {
		optimizedDeferredRenderer->numOfrenderedLights = count;
	}
}

void RenderManager::setSunLight(bool isOn)
{
	if (forwardRenderer != nullptr) {
		forwardRenderer->sunOn = isOn;
	}
	if (deferredRenderer != nullptr) {
		deferredRenderer->sunOn = isOn;
	}
	if (optimizedDeferredRenderer != nullptr) {
		optimizedDeferredRenderer->sunOn = isOn;
	}
}

double RenderManager::getGPUFrameTime()
{
	glGetQueryObjectui64v(qid[0], GL_QUERY_RESULT, &gpu_start);
	glGetQueryObjectui64v(qid[1], GL_QUERY_RESULT, &gpu_stop);
	const double total_time = (gpu_stop - gpu_start) / 1000000.0;

	return total_time;
}

void RenderManager::setCameraDirection(float cursorX, float cursorY)
{
	float cameraX = cursorX / windowWidth;
	float cameraY = cursorY / windowHeight;

	scene->activeCamera->setCameraDirection(cameraX, cameraY);
}

void RenderManager::moveCameraForward(float delta)
{
	scene->activeCamera->moveForward(delta);
}

void RenderManager::moveCameraSideways(float delta)
{
	scene->activeCamera->moveSideways(delta);
}

void RenderManager::setViewportDimensions(float width, float height)
{
	if (forwardRenderer != nullptr) {
		forwardRenderer->setViewportDimensions(width, height);
	}
	if (deferredRenderer != nullptr) {
		deferredRenderer->setViewportDimensions(width, height);
	}
	if (optimizedDeferredRenderer != nullptr) {
		optimizedDeferredRenderer->setViewportDimensions(width, height);
	}
}

void RenderManager::render()
{
	// Generate two query objects
	glGenQueries(2, qid);

	glQueryCounter(qid[0], GL_TIMESTAMP);
	if (activeRenderer != nullptr) {
		activeRenderer->render();
	}
	glQueryCounter(qid[1], GL_TIMESTAMP);
}

void RenderManager::updateScene()
{
	//nastaveni smeru slunce
	float seconds = clock() / (float)CLOCKS_PER_SEC;
	scene->directionalLights.back()->direction = glm::vec3(sin(seconds / 5.0f), cos(seconds / 5.0f), -1.0f);

	glm::vec3 pos = glm::vec3(sin(seconds / 3.0f) * 2.0f, cos(seconds / 3.0f) * 2.0f, 0.0f);
	rotatingNode->translateTo(pos);

	pos = glm::vec3(sin(seconds / 5.0f) * 4.0f, cos(seconds / 5.0f) * 4.0f, cos(seconds / 5.0f) + 1.0f);
	rotatingNode2->translateTo(pos);

	pos = glm::vec3(sin(seconds / 2.0f), cos(seconds / 2.0f), sin(seconds / 2.0f) + 2.0f);
	rotatingNode3->translateTo(pos);


}

void RenderManager::switchToRenderer(RendererType type)
{
	switch (type)
	{
	case FORWARD:
		activeRenderer = forwardRenderer;
		break;
	case DEFERRED:
		activeRenderer = deferredRenderer;
		break;
	case DEFERRED_OPTIMIZED:
		activeRenderer = optimizedDeferredRenderer;
		break;
	default:
		break;
	}
}
