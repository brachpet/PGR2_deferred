#include <glm/gtc/matrix_transform.hpp>

#include "DeferredRendererOpt.h"
#include "DeferredRenderer.h"

void DeferredRendererOpt::initShaders()
{
	geometryPassProgram.addShader(GL_VERTEX_SHADER, "shaders/optDefferedGeometry.vert");
	geometryPassProgram.addShader(GL_FRAGMENT_SHADER, "shaders/optDefferedGeometry.frag");
	geometryPassProgram.link();

	screenSpaceProgram.addShader(GL_VERTEX_SHADER, "shaders/deferredScreen.vert");
	screenSpaceProgram.addShader(GL_FRAGMENT_SHADER, "shaders/deferredScreen.frag");
	screenSpaceProgram.link();

	directionalLightProgram.addShader(GL_VERTEX_SHADER, "shaders/optDirectionalLightVolume.vert");
	directionalLightProgram.addShader(GL_FRAGMENT_SHADER, "shaders/optDirectionalLightVolume.frag");
	directionalLightProgram.link();

	pointLightProgram.addShader(GL_VERTEX_SHADER, "shaders/optPointLightVolume.vert");
	pointLightProgram.addShader(GL_FRAGMENT_SHADER, "shaders/optPointLightVolume.frag");
	pointLightProgram.link();

	forwardBlendProgram.addShader(GL_VERTEX_SHADER, "shaders/forward.vert");
	forwardBlendProgram.addShader(GL_FRAGMENT_SHADER, "shaders/forwardBlend.frag");
	forwardBlendProgram.link();
}

void DeferredRendererOpt::initGeometry()
{
	std::vector<Mesh*>::iterator it;
	for (it = scene->meshes.begin(); it != scene->meshes.end(); ++it) {
		(*it)->setupVertexArray(geometryPassProgram);
	}

	//pripravi vertex array u vsech meshu
	std::vector<Mesh*>::iterator itBlend;
	for (itBlend = scene->blendedMeshes.begin(); itBlend != scene->blendedMeshes.end(); ++itBlend) {
		(*itBlend)->setupVertexArray(forwardBlendProgram);
	}
}

void DeferredRendererOpt::initFrameBuffer()
{
	//vycisteni predchozich bufferu
	glDeleteTextures(1, &depthTexture);
	glDeleteTextures(1, &colorTexture);
	glDeleteTextures(1, &normalTexture);
	glDeleteTextures(1, &positionTexture);
	glDeleteTextures(1, &specularTexture);
	glDeleteFramebuffers(1, &frameBuffer);

	//generovani novych textur a bufferu
	glGenTextures(1, &depthTexture);
	glGenTextures(1, &colorTexture);
	glGenTextures(1, &normalTexture);
	glGenTextures(1, &positionTexture);
	glGenTextures(1, &specularTexture);
	glGenFramebuffers(1, &frameBuffer);

	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	//color buffer
	glBindTexture(GL_TEXTURE_2D, colorTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R3_G3_B2, viewportWidth, viewportHeight, 0, GL_RGB, GL_UNSIGNED_BYTE_3_3_2, NULL);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorTexture, 0);	//posledni cislo rika, do jakeho levelu mipmapy se ma generovat

	//normal buffer
	glBindTexture(GL_TEXTURE_2D, normalTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, viewportWidth, viewportHeight, 0, GL_RGB, GL_FLOAT, NULL);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, normalTexture, 0);

	//position buffer
	glBindTexture(GL_TEXTURE_2D, positionTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, viewportWidth, viewportHeight, 0, GL_RGB, GL_FLOAT, NULL);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, positionTexture, 0);

	//specular buffer
	glBindTexture(GL_TEXTURE_2D, specularTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R3_G3_B2, viewportWidth, viewportHeight, 0, GL_RGB, GL_UNSIGNED_BYTE_3_3_2, NULL);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, specularTexture, 0);

	//hlobkovy buffer
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, viewportWidth, viewportHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);	//musi mit spravny format, muzu pouzit jen GL_DEPTH_COMPONENT a nespecifikovat velikost, driver vybere nejlepsi
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);	//posledni cislo rika, do jakeho levelu mipmapy se ma generovat

	GLenum DrawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
	glDrawBuffers(4, DrawBuffers);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void DeferredRendererOpt::initLightVolumes()
{
	//smerova svetla
	std::vector<DirectionalLight*>::iterator it;
	for (it = scene->directionalLights.begin(); it != scene->directionalLights.end(); ++it) {
		(*it)->initVolume(directionalLightProgram);
	}

	//bodova svetla
	std::vector<PointLight*>::iterator itPoint;
	for (itPoint = scene->pointLights.begin(); itPoint != scene->pointLights.end(); ++itPoint) {
		(*itPoint)->initVolume(directionalLightProgram);
	}
}

void DeferredRendererOpt::geometryPass()
{
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glViewport(0, 0, viewportWidth, viewportHeight);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 view = scene->activeCamera->getViewMatrix();
	glm::mat4 projection = glm::perspective(glm::radians(scene->activeCamera->FOV), (float)viewportWidth / (float)viewportHeight, 0.1f, 300.0f);

	geometryPassProgram.use();

	//projde vsechny meshe sceny a vyrenderuje je
	std::vector<Mesh*>::iterator it;
	for (it = scene->meshes.begin(); it != scene->meshes.end(); ++it) {
		glm::mat4 model = (*it)->locationsInScene[0]->matrixToRoot;
		glm::mat4 MVP = projection * view * model;
		glUniformMatrix4fv(geometryPassProgram.shaderLocations.uniformMVP, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(geometryPassProgram.shaderLocations.uniformNormalMatrix, 1, GL_FALSE, &((*it)->locationsInScene[0]->normalMatrixToRoot[0][0]));
		glUniformMatrix4fv(geometryPassProgram.shaderLocations.uniformModelMatrix, 1, GL_FALSE, &((*it)->locationsInScene[0]->matrixToRoot[0][0]));

		if ((*it)->material->textureDiffuse != nullptr) {
			glUniform1i(geometryPassProgram.shaderLocations.textureDiffuse, 0);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, (*it)->material->textureDiffuse->textureId);
		}

		glUniform1i(geometryPassProgram.shaderLocations.useTextureOpacity, 0);
		if ((*it)->material->textureOpacity != nullptr) {
			glUniform1i(geometryPassProgram.shaderLocations.useTextureOpacity, 1);
			glUniform1i(geometryPassProgram.shaderLocations.textureOpacity, 1);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, (*it)->material->textureOpacity->textureId);
		}


		glUniform1i(geometryPassProgram.shaderLocations.useTextureNormal, 0);
		if ((*it)->material->textureNormal != nullptr) {
			glUniform1i(geometryPassProgram.shaderLocations.useTextureNormal, 1);
			glUniform1i(geometryPassProgram.shaderLocations.textureNormal, 2);
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, (*it)->material->textureNormal->textureId);
		}

		glUniform1i(geometryPassProgram.shaderLocations.useTextureSpecular, 0);
		if ((*it)->material->textureSpecular != nullptr) {
			glUniform1i(geometryPassProgram.shaderLocations.useTextureSpecular, 1);
			glUniform1i(geometryPassProgram.shaderLocations.textureSpecular, 3);
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, (*it)->material->textureSpecular->textureId);
		}

		(*it)->render();
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRendererOpt::lightPass()
{
	glDisable(GL_DEPTH_TEST);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);
	glm::vec2 windowSize = glm::vec2(viewportWidth, viewportHeight);

	//light volumes rendering
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

	//directional lights
	directionalLightProgram.use();

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(directionalLightProgram.shaderLocations.textureDiffuse, 0);
	glBindTexture(GL_TEXTURE_2D, colorTexture);

	glActiveTexture(GL_TEXTURE1);
	glUniform1i(directionalLightProgram.shaderLocations.textureNormal, 1);
	glBindTexture(GL_TEXTURE_2D, normalTexture);

	glUniform2fv(directionalLightProgram.shaderLocations.uniformWindowSize, 1, &windowSize[0]);

	GLuint lightDirection = glGetUniformLocation(directionalLightProgram.programID, "lightDirection");
	GLuint lightColor = glGetUniformLocation(directionalLightProgram.programID, "lightColor");
	if (sunOn) {
		std::vector<DirectionalLight*>::iterator itDir;
		for (itDir = scene->directionalLights.begin(); itDir != scene->directionalLights.end(); ++itDir) {
			glUniform3fv(lightDirection, 1, &((*itDir)->direction)[0]);
			glUniform3fv(lightColor, 1, &((*itDir)->lightColor)[0]);
			(*itDir)->renderVolume();
		}
	}
	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);



	//point lights
	pointLightProgram.use();

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(pointLightProgram.shaderLocations.textureDiffuse, 0);
	glBindTexture(GL_TEXTURE_2D, colorTexture);

	glActiveTexture(GL_TEXTURE1);
	glUniform1i(pointLightProgram.shaderLocations.textureNormal, 1);
	glBindTexture(GL_TEXTURE_2D, normalTexture);

	glActiveTexture(GL_TEXTURE2);
	glUniform1i(pointLightProgram.shaderLocations.texturePosition, 2);
	glBindTexture(GL_TEXTURE_2D, positionTexture);

	lightColor = glGetUniformLocation(pointLightProgram.programID, "lightColor");
	GLuint lightPosition = glGetUniformLocation(pointLightProgram.programID, "lightPosition");

	glm::mat4 view = scene->activeCamera->getViewMatrix();
	glm::mat4 projection = glm::perspective(glm::radians(scene->activeCamera->FOV), (float)viewportWidth / (float)viewportHeight, 0.1f, 300.0f);

	glm::vec3 cameraPosition = scene->activeCamera->getPositionInWordSpace();
	glUniform3fv(pointLightProgram.shaderLocations.uniformCameraPosition, 1, &cameraPosition[0]);

	glUniform2fv(pointLightProgram.shaderLocations.uniformWindowSize, 1, &windowSize[0]);

	int pointLightCounter = 0;
	std::vector<PointLight*>::iterator itPoint;
	for (itPoint = scene->pointLights.begin(); itPoint != scene->pointLights.end(); ++itPoint) {
		if (pointLightCounter == numOfrenderedLights) break;

		glm::mat4 spriteMat = (*itPoint)->getSpriteRotation(cameraPosition);
		glm::mat4 model = (*itPoint)->locationInScene->matrixToRoot;
		glm::mat4 MVP = projection * view * model * spriteMat * glm::scale(glm::mat4(1.0f), glm::vec3(3, 3, 3));
		glUniformMatrix4fv(pointLightProgram.shaderLocations.uniformMVP, 1, GL_FALSE, &MVP[0][0]);

		glUniform3fv(lightPosition, 1, &((*itPoint)->getPositionInWorldSpace())[0]);

		glUniform3fv(lightColor, 1, &((*itPoint)->lightColor)[0]);
		(*itPoint)->renderVolume();
		pointLightCounter++;
	}
	glEnable(GL_CULL_FACE);
	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

}

void DeferredRendererOpt::blendPass()
{

	//kopiruju
	glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBuffer);	//SOURCE
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); //DESTINATION
	glBlitFramebuffer(0, 0, viewportWidth, viewportHeight,	//jakou cast prvniho bufferu
		0, 0, viewportWidth, viewportHeight,	//na jakou cast druheho bfferu
		GL_DEPTH_BUFFER_BIT, GL_NEAREST);	//co a jak filtrovat
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	updatePointLightBuffer();

	//light volumes rendering
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glm::mat4 view = scene->activeCamera->getViewMatrix();
	glm::mat4 projection = glm::perspective(glm::radians(scene->activeCamera->FOV), (float)viewportWidth / (float)viewportHeight, 0.1f, 300.0f);

	DirectionalLight* sunLight = scene->directionalLights.back();

	forwardBlendProgram.use();
	std::vector<Mesh*>::iterator itBlend;
	for (itBlend = scene->blendedMeshes.begin(); itBlend != scene->blendedMeshes.end(); ++itBlend) {
		//LOG("blended\n");
		glm::mat4 model = (*itBlend)->locationsInScene[0]->matrixToRoot;
		glm::mat4 MVP = projection * view * model *glm::translate(glm::mat4(1.0f), glm::vec3(1, 1, 1));
		glUniformMatrix4fv(forwardBlendProgram.shaderLocations.uniformMVP, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(forwardBlendProgram.shaderLocations.uniformNormalMatrix, 1, GL_FALSE, &((*itBlend)->locationsInScene[0]->normalMatrixToRoot[0][0]));
		glUniformMatrix4fv(forwardBlendProgram.shaderLocations.uniformModelMatrix, 1, GL_FALSE, &((*itBlend)->locationsInScene[0]->matrixToRoot[0][0]));

		//sunlight
		GLuint lightDirectionLocation = glGetUniformLocation(forwardBlendProgram.programID, "sunLight.direction");
		glUniform3fv(lightDirectionLocation, 1, &sunLight->direction[0]);
		GLuint lightColorLocation = glGetUniformLocation(forwardBlendProgram.programID, "sunLight.color");
		if (sunOn) {
			glUniform3fv(lightColorLocation, 1, &sunLight->lightColor[0]);
		}
		else {
			glm::vec3 black = glm::vec3(0.0f);
			glUniform3fv(lightColorLocation, 1, &black[0]);
		}

		//uniform buffer tmp
		glBindBuffer(GL_UNIFORM_BUFFER, pointLightUniformBuffer);
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, pointLightUniformBuffer);
		GLuint myArrayBlockIdx = glGetUniformBlockIndex(forwardBlendProgram.programID, "PointLightsBO");
		glUniformBlockBinding(forwardBlendProgram.programID, myArrayBlockIdx, 0);
		GLuint numOfLightsLocation = glGetUniformLocation(forwardBlendProgram.programID, "numOfPointLights");
		int numOfLights = numOfrenderedLights;
		if (scene->pointLights.size() < numOfLights) numOfLights = scene->pointLights.size();
		glUniform1i(numOfLightsLocation, numOfLights);

		//cameraposition
		glm::vec3 cameraPosition = scene->activeCamera->getPositionInWordSpace();
		glUniform3fv(forwardBlendProgram.shaderLocations.uniformCameraPosition, 1, &cameraPosition[0]);



		if ((*itBlend)->material->textureDiffuse != nullptr) {
			glUniform1i(forwardBlendProgram.shaderLocations.textureDiffuse, 0);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, (*itBlend)->material->textureDiffuse->textureId);
		}

		glUniform1i(forwardBlendProgram.shaderLocations.useTextureOpacity, 0);
		if ((*itBlend)->material->textureOpacity != nullptr) {
			glUniform1i(forwardBlendProgram.shaderLocations.useTextureOpacity, 1);
			glUniform1i(forwardBlendProgram.shaderLocations.textureOpacity, 1);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, (*itBlend)->material->textureOpacity->textureId);
		}


		glUniform1i(forwardBlendProgram.shaderLocations.useTextureNormal, 0);
		if ((*itBlend)->material->textureNormal != nullptr) {
			glUniform1i(forwardBlendProgram.shaderLocations.useTextureNormal, 1);
			glUniform1i(forwardBlendProgram.shaderLocations.textureNormal, 2);
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, (*itBlend)->material->textureNormal->textureId);
		}

		glUniform1i(forwardBlendProgram.shaderLocations.useTextureSpecular, 0);
		if ((*itBlend)->material->textureSpecular != nullptr) {
			glUniform1i(forwardBlendProgram.shaderLocations.useTextureSpecular, 1);
			glUniform1i(forwardBlendProgram.shaderLocations.textureSpecular, 3);
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, (*itBlend)->material->textureSpecular->textureId);
		}

		(*itBlend)->render();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glUseProgram(0);

	glDisable(GL_BLEND);
}

void DeferredRendererOpt::updatePointLightBuffer()
{
	std::vector<PointLight *> lights = scene->pointLights;

	int numOfLights = numOfrenderedLights;
	if (lights.size() < numOfLights) numOfLights = lights.size();

	glm::vec4 * lightPositions = new glm::vec4[numOfLights];
	glm::vec4 * lightColors = new glm::vec4[numOfLights];

	int i = 0;
	for each (PointLight * light in lights)
	{
		if (i == numOfrenderedLights) break;

		lightPositions[i] = light->getPositionInWorldSpace();
		lightColors[i] = glm::vec4(light->lightColor, 0.0f);
		i++;
	}

	glBindBuffer(GL_UNIFORM_BUFFER, pointLightUniformBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, numOfLights * 4 * sizeof(GLfloat), &lightPositions[0].x);
	glBufferSubData(GL_UNIFORM_BUFFER, MAX_POINT_LIGHTS_COUNT * 4 * sizeof(GLfloat), numOfLights * 4 * sizeof(GLfloat), &lightColors[0].x);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	delete[] lightPositions;
}

DeferredRendererOpt::DeferredRendererOpt(Scene * scene) : Renderer(scene), geometryPassProgram(ShaderProgram()), screenSpaceProgram(ShaderProgram()), directionalLightProgram(ShaderProgram()), pointLightProgram(ShaderProgram()), forwardBlendProgram(ShaderProgram())
{
}

DeferredRendererOpt::DeferredRendererOpt() : Renderer(), geometryPassProgram(ShaderProgram()), screenSpaceProgram(ShaderProgram()), directionalLightProgram(ShaderProgram()), pointLightProgram(ShaderProgram()), forwardBlendProgram(ShaderProgram())
{
}

DeferredRendererOpt::~DeferredRendererOpt()
{
}

void DeferredRendererOpt::init()
{
	initShaders();
	initGeometry();
	initFrameBuffer();
	initLightVolumes();

	glEnable(GL_CULL_FACE);
	glClearDepth(1.0f);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	

	glGenVertexArrays(1, &screenVertexArrayObject);
	glGenBuffers(1, &screenVertexBufferObject);
	glGenBuffers(1, &screenElementBufferObject);

	glBindBuffer(GL_ARRAY_BUFFER, screenVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(screenVertices), screenVertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, screenElementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * 2, screenIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(screenVertexArrayObject);
	glBindBuffer(GL_ARRAY_BUFFER, screenVertexBufferObject);

	//position pointer
	if (screenSpaceProgram.shaderLocations.attributPosition != -1) {
		glEnableVertexAttribArray(screenSpaceProgram.shaderLocations.attributPosition);
		glVertexAttribPointer(screenSpaceProgram.shaderLocations.attributPosition, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}

	//unbiding
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}

void DeferredRendererOpt::render()
{
	geometryPass();
	lightPass();
	blendPass();
}

void DeferredRendererOpt::setViewportDimensions(int width, int height)
{
	viewportWidth = width;
	viewportHeight = height;

	initFrameBuffer();

}
