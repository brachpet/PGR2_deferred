#include "Renderer.h"
#include "common.h"

Renderer::Renderer(Scene * scene) : scene(scene), viewportWidth(WINDOW_WIDTH), viewportHeight(WINDOW_HEIGHT)
{
}

Renderer::Renderer() : scene(new Scene()), viewportWidth(WINDOW_WIDTH), viewportHeight(WINDOW_HEIGHT)
{
}

Renderer::~Renderer()
{
	//delete scene;
}

void Renderer::setViewportDimensions(int width, int height)
{
	viewportWidth  = width;
	viewportHeight = height;
}
