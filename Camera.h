#pragma once
#include "SceneNode.h"


class CameraNode : public SceneNode {

public:
	float verticalAxisTop;
	float verticalAxisBottom;

	glm::mat4 inverseMatrixToRoot;
	glm::vec3 direction;
	float FOV;						///field of view in degrees
	float sensitivity;

	CameraNode(SceneNode * parent);
	CameraNode();
	~CameraNode();

	void computeInverseMatrix();
	virtual void computeMatrixToRoot();

	void setCameraDirection(glm::vec3 direction);
	void setCameraDirection(float windowX, float windowY);	/// windowX and windowY is position of cursor in window
	void moveForward(float delta);
	void moveSideways(float delta);

	glm::mat4 getViewMatrix() const;

	glm::vec3 getPositionInWordSpace() const;

	void print();
};