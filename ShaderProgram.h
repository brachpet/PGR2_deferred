#pragma once
#include <vector>

#include "Shader.h"

struct ShaderLocations {
	GLuint uniformMVP;
	GLuint uniformNormalMatrix;
	GLuint uniformModelMatrix;
	GLuint uniformCameraPosition;
	GLuint uniformWindowSize;

	GLuint textureDiffuse;
	GLuint textureOpacity;
	GLuint textureNormal;
	GLuint textureSpecular;
	GLuint texturePosition;
	GLuint textureMatNormal;
	GLuint textureUVs;
	GLuint textureTangent;

	GLuint useTextureOpacity;
	GLuint useTextureNormal;
	GLuint useTextureSpecular;

	GLuint attributPosition;
	GLuint attributNormal;
	GLuint attributUVs;
	GLuint attributTangent;
};

class ShaderProgram
{
	std::vector<Shader*> shaders;
public:
	ShaderLocations shaderLocations;
	GLuint programID;

public:
	ShaderProgram();
	~ShaderProgram();

	void addShader(GLenum shaderType, const char * shaderFilePath);
	void addShader(Shader * shader);

	void link();
	void initShaderLocations();
	void use();
};