#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>

#include "common.h"
#include "Material.h"

Material::Material()
{
}

Material::~Material()
{
	if (textureDiffuse != nullptr)
		delete textureDiffuse;
	if (textureNormal != nullptr)
		delete textureNormal;
	if (textureSpecular != nullptr)
		delete textureSpecular;
	if (textureOpacity != nullptr)
		delete textureOpacity;
}

void Material::loadFromFile(const char * path)
{
	Assimp::Importer importer;

	const aiScene * scn = importer.ReadFile(path, 0
		| aiProcess_Triangulate
		| aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
		| aiProcess_GenSmoothNormals        // Calculate normals per vertex.
		| aiProcess_GenUVCoords
		| aiProcess_JoinIdenticalVertices
		| aiProcess_CalcTangentSpace
	);

	if (!scn) {
		LOG("Failed to init scene from file \n");
		return;
	}

	if (scn->mNumMeshes == 0) {
		LOG("Scene contains zero meshes \n");
		return;
	}

	int numberOfMaterials = scn->mNumMaterials;
	LOG("num of materials: ");
	LOG(numberOfMaterials);
	LOG("\n");

	const aiMaterial * assimpMaterial = scn->mMaterials[1];

	aiString texturePath;

	unsigned int numTextures = assimpMaterial->GetTextureCount(aiTextureType_DIFFUSE);
	if (assimpMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0 && assimpMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == AI_SUCCESS)
	{
		Texture * texture = new Texture(texturePath.C_Str());
		textureDiffuse = texture;
	}

	LOG("num of diffuse textures: ");
	LOG(numTextures);
	LOG("\n");

	numTextures = assimpMaterial->GetTextureCount(aiTextureType_HEIGHT);
	if (assimpMaterial->GetTextureCount(aiTextureType_HEIGHT) > 0 && assimpMaterial->GetTexture(aiTextureType_HEIGHT, 0, &texturePath) == AI_SUCCESS)
	{
		Texture * texture = new Texture(texturePath.C_Str());
		textureNormal = texture;
	}
	else {
		LOG("num of normal textures: ");
		LOG(numTextures);
		LOG("\n");
	}

	numTextures = assimpMaterial->GetTextureCount(aiTextureType_OPACITY);
	if (assimpMaterial->GetTextureCount(aiTextureType_OPACITY) > 0 && assimpMaterial->GetTexture(aiTextureType_OPACITY, 0, &texturePath) == AI_SUCCESS)
	{
		Texture * texture = new Texture(texturePath.C_Str());
		textureOpacity = texture;
	}
	else {
		LOG("num of opacity textures: ");
		LOG(numTextures);
		LOG("\n");
	}

	numTextures = assimpMaterial->GetTextureCount(aiTextureType_SPECULAR);
	if (assimpMaterial->GetTextureCount(aiTextureType_SPECULAR) > 0 && assimpMaterial->GetTexture(aiTextureType_SPECULAR, 0, &texturePath) == AI_SUCCESS)
	{
		Texture * texture = new Texture(texturePath.C_Str());
		textureSpecular = texture;
	}
	else {
		LOG("num of specular textures: ");
		LOG(numTextures);
		LOG("\n");
	}
}
