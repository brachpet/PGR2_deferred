#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <windows.h>

#include "common.h"
#include "ForwardRenderer.h"

void ForwardRenderer::initShaders()
{
	forwardProgram.addShader(GL_VERTEX_SHADER, "shaders/forward.vert");
	forwardProgram.addShader(GL_FRAGMENT_SHADER, "shaders/forward.frag");
	forwardProgram.link();

	forwardBlendProgram.addShader(GL_VERTEX_SHADER, "shaders/forward.vert");
	forwardBlendProgram.addShader(GL_FRAGMENT_SHADER, "shaders/forwardBlend.frag");
	forwardBlendProgram.link();
}

void ForwardRenderer::initGeometry()
{
	//pripravi vertex array u vsech meshu
	std::vector<Mesh*>::iterator it;
	for (it = scene->meshes.begin(); it != scene->meshes.end(); ++it) {
		(*it)->setupVertexArray(forwardProgram);
	}

	//pripravi vertex array u vsech meshu
	std::vector<Mesh*>::iterator itBlend;
	for (itBlend = scene->blendedMeshes.begin(); itBlend != scene->blendedMeshes.end(); ++itBlend) {
		(*itBlend)->setupVertexArray(forwardBlendProgram);
	}
}

void ForwardRenderer::updatePointLightBuffer()
{
	std::vector<PointLight *> lights = scene->pointLights;

	int numOfLights = numOfrenderedLights;
	if (lights.size() < numOfLights) numOfLights = lights.size();

	glm::vec4 * lightPositions = new glm::vec4[numOfLights];
	glm::vec4 * lightColors = new glm::vec4[numOfLights];

	int i = 0;
	for each (PointLight * light in lights)
	{
		if (i == numOfrenderedLights) break;

		lightPositions[i] = light->getPositionInWorldSpace();
		lightColors[i] = glm::vec4(light->lightColor, 0.0f);
		i++;
	}

	glBindBuffer(GL_UNIFORM_BUFFER, pointLightUniformBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, numOfLights * 4 * sizeof(GLfloat), &lightPositions[0].x);
	glBufferSubData(GL_UNIFORM_BUFFER, MAX_POINT_LIGHTS_COUNT * 4 * sizeof(GLfloat), numOfLights * 4 * sizeof(GLfloat), &lightColors[0].x);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	delete[] lightPositions;
}


ForwardRenderer::ForwardRenderer(Scene * scene) : Renderer(scene), forwardProgram(ShaderProgram())
{
}

ForwardRenderer::ForwardRenderer() : Renderer(), forwardProgram(ShaderProgram()), forwardBlendProgram(ShaderProgram())
{
}

ForwardRenderer::~ForwardRenderer()
{
}

void ForwardRenderer::init()
{
	initShaders();
	initGeometry();

	glEnable(GL_CULL_FACE);
	glClearDepth(1.0f);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	

	//uniform buffer pro bodova svetla
	if (pointLightUniformBuffer == 0) {
		glGenBuffers(1, &pointLightUniformBuffer);
	}
	glBindBuffer(GL_UNIFORM_BUFFER, pointLightUniformBuffer);
	glBufferData(GL_UNIFORM_BUFFER, MAX_POINT_LIGHTS_COUNT * 8 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

}

void ForwardRenderer::render()
{
	updatePointLightBuffer();

	glViewport(0, 0, viewportWidth, viewportHeight);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 view = scene->activeCamera->getViewMatrix();
	glm::mat4 projection = glm::perspective(glm::radians(scene->activeCamera->FOV), (float)viewportWidth / (float)viewportHeight, 0.1f, 300.0f);

	DirectionalLight* sunLight = scene->directionalLights.back();
	
	forwardProgram.use();

	//projde vsechny meshe sceny a vyrenderuje je
	std::vector<Mesh*>::iterator it;
	for (it = scene->meshes.begin(); it != scene->meshes.end(); ++it) {
		glm::mat4 model = (*it)->locationsInScene[0]->matrixToRoot;
		glm::mat4 MVP = projection * view * model; // *glm::translate(glm::mat4(1.0f), glm::vec3(1, 1, 1));
		glUniformMatrix4fv(forwardProgram.shaderLocations.uniformMVP, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(forwardProgram.shaderLocations.uniformNormalMatrix, 1, GL_FALSE, &((*it)->locationsInScene[0]->normalMatrixToRoot[0][0]));
		glUniformMatrix4fv(forwardProgram.shaderLocations.uniformModelMatrix, 1, GL_FALSE, &((*it)->locationsInScene[0]->matrixToRoot[0][0]));

		//sunlight
		GLuint lightDirectionLocation = glGetUniformLocation(forwardProgram.programID, "sunLight.direction");
		glUniform3fv(lightDirectionLocation, 1, &sunLight->direction[0]);
		GLuint lightColorLocation = glGetUniformLocation(forwardProgram.programID, "sunLight.color");
		if (sunOn) {
			glUniform3fv(lightColorLocation, 1, &sunLight->lightColor[0]);
		}
		else {
			glm::vec3 black = glm::vec3(0.0f);
			glUniform3fv(lightColorLocation, 1, &black[0]);
		}

		//uniform buffer tmp
		glBindBuffer(GL_UNIFORM_BUFFER, pointLightUniformBuffer);
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, pointLightUniformBuffer);
		GLuint myArrayBlockIdx = glGetUniformBlockIndex(forwardProgram.programID, "PointLightsBO");
		glUniformBlockBinding(forwardProgram.programID, myArrayBlockIdx, 0);
		GLuint numOfLightsLocation = glGetUniformLocation(forwardProgram.programID, "numOfPointLights");
		int numOfLights = numOfrenderedLights;
		if (scene->pointLights.size() < numOfLights) numOfLights = scene->pointLights.size();
		glUniform1i(numOfLightsLocation, numOfLights);

		//cameraposition
		glm::vec3 cameraPosition = scene->activeCamera->getPositionInWordSpace();
		glUniform3fv(forwardProgram.shaderLocations.uniformCameraPosition, 1, &cameraPosition[0]);



		if ((*it)->material->textureDiffuse != nullptr) {
			glUniform1i(forwardProgram.shaderLocations.textureDiffuse, 0);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, (*it)->material->textureDiffuse->textureId);
		}

		glUniform1i(forwardProgram.shaderLocations.useTextureOpacity, 0);
		if ((*it)->material->textureOpacity != nullptr) {
			glUniform1i(forwardProgram.shaderLocations.useTextureOpacity, 1);
			glUniform1i(forwardProgram.shaderLocations.textureOpacity, 1);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, (*it)->material->textureOpacity->textureId);
		}


		glUniform1i(forwardProgram.shaderLocations.useTextureNormal, 0);
		if ((*it)->material->textureNormal != nullptr) {
			glUniform1i(forwardProgram.shaderLocations.useTextureNormal, 1);
			glUniform1i(forwardProgram.shaderLocations.textureNormal, 2);
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, (*it)->material->textureNormal->textureId);
		}

		glUniform1i(forwardProgram.shaderLocations.useTextureSpecular, 0);
		if ((*it)->material->textureSpecular != nullptr) {
			glUniform1i(forwardProgram.shaderLocations.useTextureSpecular, 1);
			glUniform1i(forwardProgram.shaderLocations.textureSpecular, 3);
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, (*it)->material->textureSpecular->textureId);
		}

		(*it)->render();
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	glUseProgram(0);
	

	//light volumes rendering
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	forwardBlendProgram.use();
	std::vector<Mesh*>::iterator itBlend;
	for (itBlend = scene->blendedMeshes.begin(); itBlend != scene->blendedMeshes.end(); ++itBlend) {
		//LOG("blended\n");
		glm::mat4 model = (*itBlend)->locationsInScene[0]->matrixToRoot;
		glm::mat4 MVP = projection * view * model *glm::translate(glm::mat4(1.0f), glm::vec3(1, 1, 1));
		glUniformMatrix4fv(forwardBlendProgram.shaderLocations.uniformMVP, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(forwardBlendProgram.shaderLocations.uniformNormalMatrix, 1, GL_FALSE, &((*itBlend)->locationsInScene[0]->normalMatrixToRoot[0][0]));
		glUniformMatrix4fv(forwardBlendProgram.shaderLocations.uniformModelMatrix, 1, GL_FALSE, &((*itBlend)->locationsInScene[0]->matrixToRoot[0][0]));

		//sunlight
		GLuint lightDirectionLocation = glGetUniformLocation(forwardBlendProgram.programID, "sunLight.direction");
		glUniform3fv(lightDirectionLocation, 1, &sunLight->direction[0]);
		GLuint lightColorLocation = glGetUniformLocation(forwardBlendProgram.programID, "sunLight.color");
		if (sunOn) {
			glUniform3fv(lightColorLocation, 1, &sunLight->lightColor[0]);
		}
		else {
			glm::vec3 black = glm::vec3(0.0f);
			glUniform3fv(lightColorLocation, 1, &black[0]);
		}

		//uniform buffer tmp
		glBindBuffer(GL_UNIFORM_BUFFER, pointLightUniformBuffer);
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, pointLightUniformBuffer);
		GLuint myArrayBlockIdx = glGetUniformBlockIndex(forwardBlendProgram.programID, "PointLightsBO");
		glUniformBlockBinding(forwardBlendProgram.programID, myArrayBlockIdx, 0);
		GLuint numOfLightsLocation = glGetUniformLocation(forwardBlendProgram.programID, "numOfPointLights");
		int numOfLights = numOfrenderedLights;
		if (scene->pointLights.size() < numOfLights) numOfLights = scene->pointLights.size();
		glUniform1i(numOfLightsLocation, numOfLights);

		//cameraposition
		glm::vec3 cameraPosition = scene->activeCamera->getPositionInWordSpace();
		glUniform3fv(forwardBlendProgram.shaderLocations.uniformCameraPosition, 1, &cameraPosition[0]);



		if ((*itBlend)->material->textureDiffuse != nullptr) {
			glUniform1i(forwardBlendProgram.shaderLocations.textureDiffuse, 0);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, (*itBlend)->material->textureDiffuse->textureId);
		}

		glUniform1i(forwardBlendProgram.shaderLocations.useTextureOpacity, 0);
		if ((*itBlend)->material->textureOpacity != nullptr) {
			glUniform1i(forwardBlendProgram.shaderLocations.useTextureOpacity, 1);
			glUniform1i(forwardBlendProgram.shaderLocations.textureOpacity, 1);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, (*itBlend)->material->textureOpacity->textureId);
		}


		glUniform1i(forwardBlendProgram.shaderLocations.useTextureNormal, 0);
		if ((*itBlend)->material->textureNormal != nullptr) {
			glUniform1i(forwardBlendProgram.shaderLocations.useTextureNormal, 1);
			glUniform1i(forwardBlendProgram.shaderLocations.textureNormal, 2);
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, (*itBlend)->material->textureNormal->textureId);
		}

		glUniform1i(forwardBlendProgram.shaderLocations.useTextureSpecular, 0);
		if ((*itBlend)->material->textureSpecular != nullptr) {
			glUniform1i(forwardBlendProgram.shaderLocations.useTextureSpecular, 1);
			glUniform1i(forwardBlendProgram.shaderLocations.textureSpecular, 3);
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, (*itBlend)->material->textureSpecular->textureId);
		}

		(*itBlend)->render();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glUseProgram(0);

	glDisable(GL_BLEND);
}