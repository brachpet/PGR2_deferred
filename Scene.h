#pragma once
#include <vector>

#include "SceneNode.h"
#include "Camera.h"
#include "Mesh.h"
#include "Material.h"
#include "Light.h"

class Scene {
public:
	CameraNode* activeCamera;
	SceneNode* sceneRoot;

	//resource pools
	std::vector<Mesh*>				meshes;
	std::vector<Mesh*>				blendedMeshes;
	std::vector<Material*>			materials;
	std::vector<PointLight*>		pointLights;
	std::vector<DirectionalLight*>	directionalLights;

	void loadFromFile(const char * path);

	Scene();
	~Scene();
};