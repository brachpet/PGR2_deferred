#pragma once
#include "GL/glew.h"
#include <string>

using namespace std;

class Shader
{
	string shaderCode;
	const char * shaderFilePath;

public:
	GLenum shaderType;
	GLuint shaderID;


public:
	Shader(GLenum shaderType, const char * shaderFilePath);
	Shader();
	~Shader();

	bool init();
	void compile();
	GLuint getShaderID() const;	//const means it will not modify shaderID
	void print() const;
};