#include "Shader.h"
#include <fstream>
#include <vector>

#include "common.h"

using namespace std;

Shader::Shader(GLenum shaderType, const char * shaderFilePath) : shaderType(shaderType), shaderFilePath(shaderFilePath)
{
	shaderID = glCreateShader(shaderType);
}

Shader::Shader()
{
}

Shader::~Shader()
{
	glDeleteShader(shaderID);

	LOG("deleting Shader \n");
}

bool Shader::init()
{
	ifstream shaderStream(shaderFilePath, ifstream::in);
	if (shaderStream.is_open()) {
		string line = "";
		while (getline(shaderStream, line))
			shaderCode += "\n" + line;

		shaderStream.close();
	}
	else {
		LOG("Could not read shader source file ");
		LOG(shaderFilePath);
		LOG("\n");
		return false;
	}

	LOG("creating Shader, id: ");
	LOG(shaderID);
	LOG("\n");
	return true;
}

void Shader::compile()
{
	char const * sourcePointer = shaderCode.c_str();
	glShaderSource(shaderID, 1, &sourcePointer, NULL);
	glCompileShader(shaderID);

#ifdef _DEBUG
	GLint result = GL_FALSE;
	int infoLogLength;

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 1) {
		vector<char> shaderErrorMessage(infoLogLength + 1);
		glGetShaderInfoLog(shaderID, infoLogLength, NULL, &shaderErrorMessage[0]);
		LOG(&shaderErrorMessage[0]);
		LOG("\n");
	}
	else {
		LOG("shader compilation ok\n");
	}
#endif // _DEBUG

}

GLuint Shader::getShaderID() const
{
	return shaderID;
}

void Shader::print() const
{
	LOG(shaderCode.c_str());
	LOG("\n");
}
