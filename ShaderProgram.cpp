#include "ShaderProgram.h"
#include "common.h"

ShaderProgram::ShaderProgram()
{
	programID = glCreateProgram();


	LOG("creating ShaderProgram ");
	LOG(programID);
	LOG("\n");

}

ShaderProgram::~ShaderProgram()
{
	LOG("deleting ShaderProgram ");
	LOG(programID);
	LOG("\n");
	std::vector<Shader*>::iterator it;
	for (it = shaders.begin(); it != shaders.end(); ++it) {
		delete *it;
	}
}


void ShaderProgram::addShader(GLenum shaderType, const char * shaderFilePath)
{
	Shader * shader = new Shader(shaderType, shaderFilePath);
	shader->init();
	shader->compile();
	shaders.push_back(shader);
}

void ShaderProgram::addShader(Shader * shader)
{
	shaders.push_back(shader);
}

void ShaderProgram::link()
{

	LOG("Linking program \n");

	//attach all shaders in list
	std::vector<Shader*>::iterator it;
	for (it = shaders.begin(); it != shaders.end(); ++it) {
		glAttachShader(programID, (*it)->shaderID);
	}

	glLinkProgram(programID);

#ifdef _DEBUG
	// error check
	GLint result = GL_FALSE;
	int infoLogLength;

	glGetProgramiv(programID, GL_LINK_STATUS, &result);
	glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		vector<char> ProgramErrorMessage(infoLogLength + 1);
		glGetProgramInfoLog(programID, infoLogLength, NULL, &ProgramErrorMessage[0]);
		LOG(&ProgramErrorMessage[0]);
		LOG("\n");
	}
	else {
		LOG("shader program linking ok\n");
	}
#endif // _DEBUG

	//detach all shaders in list
	for (it = shaders.begin(); it != shaders.end(); ++it) {
		glDetachShader(programID, (*it)->shaderID);
	}
	initShaderLocations();
}

void ShaderProgram::initShaderLocations()
{
	//matrices
	shaderLocations.uniformMVP			= glGetUniformLocation(programID, "MVP");
	shaderLocations.uniformNormalMatrix = glGetUniformLocation(programID, "normalMatrix");
	shaderLocations.uniformModelMatrix  = glGetUniformLocation(programID, "modelMatrix");

	//misc
	shaderLocations.uniformCameraPosition = glGetUniformLocation(programID, "cameraPosition");
	shaderLocations.uniformWindowSize     = glGetUniformLocation(programID, "windowSize");

	//textures
	shaderLocations.textureDiffuse  = glGetUniformLocation(programID, "textureDiffuse");
	shaderLocations.textureOpacity  = glGetUniformLocation(programID, "textureOpacity");
	shaderLocations.textureNormal   = glGetUniformLocation(programID, "textureNormal");
	shaderLocations.textureSpecular = glGetUniformLocation(programID, "textureSpecular");
	shaderLocations.texturePosition = glGetUniformLocation(programID, "texturePosition");
	shaderLocations.textureMatNormal = glGetUniformLocation(programID, "textureMatNormal");
	shaderLocations.textureUVs      = glGetUniformLocation(programID, "textureUvs");
	shaderLocations.textureTangent  = glGetUniformLocation(programID, "textureTangent");

	//flags
	shaderLocations.useTextureOpacity  = glGetUniformLocation(programID, "useOpacity");
	shaderLocations.useTextureNormal   = glGetUniformLocation(programID, "useNormal");
	shaderLocations.useTextureSpecular = glGetUniformLocation(programID, "useSpecular");

	//attributes
	shaderLocations.attributPosition = glGetAttribLocation(programID, "position");
	shaderLocations.attributNormal   = glGetAttribLocation(programID, "normal");
	shaderLocations.attributUVs      = glGetAttribLocation(programID, "uvs");
	shaderLocations.attributTangent  = glGetAttribLocation(programID, "tangent");
}

void ShaderProgram::use()
{
	glUseProgram(programID);
}
