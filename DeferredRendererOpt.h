#pragma once
#include "Renderer.h"

class DeferredRendererOpt : public Renderer {
	ShaderProgram geometryPassProgram;
	ShaderProgram screenSpaceProgram;
	ShaderProgram directionalLightProgram;
	ShaderProgram pointLightProgram;
	ShaderProgram forwardBlendProgram;

	GLuint pointLightUniformBuffer = 0;

	GLuint frameBuffer = 0;
	GLuint depthTexture = 0;
	GLuint colorTexture = 0;
	GLuint normalTexture = 0;
	GLuint positionTexture = 0;
	GLuint specularTexture = 0;

	GLuint screenVertexArrayObject;
	GLuint screenVertexBufferObject;
	GLuint screenElementBufferObject;


	void initShaders();
	void initGeometry();
	void initFrameBuffer();
	void initLightVolumes();

	void geometryPass();
	void lightPass();
	void blendPass();

	void updatePointLightBuffer();


public:
	DeferredRendererOpt(Scene * scene);
	DeferredRendererOpt();
	~DeferredRendererOpt();

	void init();
	void render();

	void setViewportDimensions(int width, int height);
};