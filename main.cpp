//libraries
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <glm/gtc/matrix_transform.hpp>
#include <IL/il.h>
#include <AntTweakBar.h>

//std
#include <iostream>

//aplication
#include "common.h"
#include "RenderManager.h"


RenderManager* renderManager;

bool sunOn = true;

RendererType useRenderer = FORWARD;

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	TwEventMouseButtonGLFW(button, action);
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	TwEventMousePosGLFW(xpos, ypos);
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
	renderManager->setViewportDimensions(width, height);
}

int main() {

	GLFWwindow* window;
	if (!glfwInit()) {
		return -1;
	}
	else {
		LOG("uspesna inicializace glfw \n");
	}


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);  //opengl version 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "PGR2", NULL, NULL);	//windowed

	GLFWmonitor* myMonitor = glfwGetPrimaryMonitor();
	//monitor parameters
	const GLFWvidmode* mode = glfwGetVideoMode(myMonitor);

	if (!window)
	{
		glfwTerminate();
		return -1;
	}
	else {
		LOG("uspesna inicializace okna \n");
	}

	// Make the window's context current
	glfwMakeContextCurrent(window);

	glewExperimental = true; // Needed in core profile
	if (glewInit() != GLEW_OK) {
		return -1;
	}
	else {
		LOG("uspesna inicializace glew \n");
	}	

	//devil init
	ilInit();

	//render manager
	renderManager = new RenderManager();
	renderManager->init();


	//tweakBar gui init
	TwInit(TW_OPENGL_CORE, NULL);
	TwWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	TwBar *myBar = TwNewBar("Renderer info");
	//nastaveni gui okna
	TwDefine(" 'Renderer info' size='200 140' ");
	TwDefine(" 'Renderer info' color='100 100 100' alpha=128 ");
	TwDefine(" 'Renderer info' resizable=false ");

	int numOfRenderedLights = 15;
	TwAddVarRW(myBar, "Point lights", TW_TYPE_INT32, &numOfRenderedLights, " min=0 max=256 step=1 group='Render setup' ");
	TwAddVarRW(myBar, "Sun", TW_TYPE_BOOLCPP, &sunOn, " group='Render setup' ");
	{
		// ShapeEV associates Shape enum values with labels that will be displayed instead of enum values
		TwEnumVal renderersEV[3] = { { FORWARD, "forward" },{ DEFERRED, "deferred" },{ DEFERRED_OPTIMIZED, "deferred optimized" } };
		// Create a type for the enum shapeEV
		TwType rendererType = TwDefineEnum("Renderer", renderersEV, 3);
		// add 'g_CurrentShape' to 'bar': this is a variable of type ShapeType. Its key shortcuts are [<] and [>].
		TwAddVarRW(myBar, "Renderer", rendererType, &useRenderer, " group='Render setup' keyIncr='<' keyDecr='>' help='Change object shape.' ");
	}
	double renderTime = 0.0;
	TwAddVarRO(myBar, "GPU time", TW_TYPE_DOUBLE, &renderTime, " group='Performance info' ");


	//tweakBar gui callback
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
	glfwSetWindowSizeCallback(window, window_size_callback);


	glEnable(GL_DOUBLEBUFFER);

	bool cameraMode = false;

	while (!glfwWindowShouldClose(window))
	{
		//rozmery okna a prizpusobeni gui
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		int windowWidth = width;
		int windowHeight = height;
		TwWindowSize(windowWidth, windowHeight);
		
		//settng rendered lights
		renderManager->setNumOfRenderedLights(numOfRenderedLights);
		renderManager->setSunLight(sunOn);
		
		//camera orientation
		if (cameraMode) {
			double xpos, ypos;
			glfwGetCursorPos(window, &xpos, &ypos);
			renderManager->setCameraDirection(xpos, ypos);
		}

		//choosing active renderer
		renderManager->switchToRenderer(useRenderer);

		//ticking
		renderManager->updateScene();

		//rendering
		renderManager->render();

		renderTime = renderManager->getGPUFrameTime();

		TwDraw();

		glfwSwapBuffers(window);

		glfwPollEvents();

		// zavreni okna na klavesu ESC
		if (glfwGetKey(window, GLFW_KEY_ESCAPE)) {
			glfwSetWindowShouldClose(window, 1);
		}

		//toggle fullscreen
		if (glfwGetKey(window, GLFW_KEY_PAGE_UP)) {
			glfwSetWindowMonitor(window, myMonitor, 0, 0, mode->width, mode->height, mode->refreshRate);
		}

		//toggle windowed mode
		if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN)) {
			glfwSetWindowMonitor(window, NULL, 100, 100, WINDOW_WIDTH, WINDOW_HEIGHT, mode->refreshRate);
		}

		//toggle cursor mode
		
		//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		if (glfwGetKey(window, GLFW_KEY_C)) {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			cameraMode = true;
		}
		if (glfwGetKey(window, GLFW_KEY_V)) {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			cameraMode = false;
		}

		//camera manipulation
		if (glfwGetKey(window, GLFW_KEY_W)) {
			renderManager->moveCameraForward(0.05f);
		}
		if (glfwGetKey(window, GLFW_KEY_S)) {
			renderManager->moveCameraForward(-0.05f);
		}
		if (glfwGetKey(window, GLFW_KEY_A)) {
			renderManager->moveCameraSideways(-0.05f);
		}
		if (glfwGetKey(window, GLFW_KEY_D)) {
			renderManager->moveCameraSideways(0.05f);
		}

	}

	delete renderManager;

	TwTerminate();
	glfwTerminate();
}